﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63110513_Seminar2
{
    public partial class Default : System.Web.UI.Page
    {

        private List<HRMService.DepartmentEntity> departments;
        private List<HRMService.EmployeeEntity> employees;
        private List<HRMService.OrderEntity> orders;
        
        protected void Page_Load(object sender, EventArgs e)
        {

            var service = new HRMService.HRMServiceClient();
            this.departments = service.GetDepartmentList();
            this.employees = service.GetEmployeeList();

            if (!IsPostBack)
            {
                fill_DDL_DepartmentGroupName();
                fill_DDL_Department();
                
            }

        }

        

        protected void DDL_DepartmentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            DV_EmployeeDetails.Visible = false;
            GV_Employee.SelectedIndex = -1;
            B_Orders.Visible = false;
            orderTagsVisibility(false);

            fill_DDL_Department();
        }

        protected void DDL_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            DV_EmployeeDetails.Visible = false;
            GV_Employee.SelectedIndex = -1;
            B_Orders.Visible = false;
            orderTagsVisibility(false);
        }

        protected void GV_Employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            fill_DV_EmployeeDetails();

            DV_EmployeeDetails.Visible = true;
            orderTagsVisibility(false);

            if (DDL_Department.SelectedItem.Text.Equals("Sales"))
                B_Orders.Visible = true;
            else
                B_Orders.Visible = false;
        }

        protected void GV_Employee_PageIndexChanging(object sender, EventArgs e)
        {
            DV_EmployeeDetails.Visible = false;
            GV_Employee.SelectedIndex = -1;
        }

        protected void B_Orders_Click(object sender, EventArgs e)
        {
            processOrders();

            orderTagsVisibility(true);
        }

        private void fill_DDL_DepartmentGroupName()
        {
            var query = this.departments.Select(x => x.DepartmentGroupName).Distinct().ToList();

            DDL_DepartmentGroup.DataSource = query;
            DDL_DepartmentGroup.DataBind();
        }

        private void fill_DDL_Department()
        {
            String selectedDepartment = DDL_DepartmentGroup.SelectedValue;

            var query = from department in this.departments
                        where department.DepartmentGroupName == selectedDepartment
                        select department;

            DDL_Department.DataSource = query;
            DDL_Department.DataTextField = "DepartmentName";
            DDL_Department.DataValueField = "DepartmentID";
            DDL_Department.DataBind();
        }

        private void fill_DV_EmployeeDetails()
        {
            int employeeID = (int)GV_Employee.SelectedValue;
            var service = new HRMService.HRMServiceClient();
            var query = from e in service.GetEmployeeList()
                        where e.EmployeeID == employeeID
                        select new
                        {
                            EmployeeID = e.EmployeeID,
                            FirstName = e.FirstName,
                            LastName = e.LastName,
                            Gender = e.Gender,
                            BirthDate = e.BirthDate,
                            PhoneNumber = e.PhoneNumber,
                            EmailAdress = e.EmailAddress,
                            Address = e.Address,
                            City = e.City,
                            PostalCode = e.PostalCode,
                            State = e.State,
                            Country = e.Country,
                            JobTitle = e.JobTitle,
                            HireDate = e.HireDate,
                            DepartmentID = e.DepartmentID,
                            Shift = e.Shift,
                            ShiftStartTime = e.ShiftStartTime,
                            ShiftEndTime = e.ShiftEndTime,
                            PayRate = e.PayRate,
                            WorkYears = (int)((DateTime.Today - e.HireDate).Days / 365.25)
                        };

            DV_EmployeeDetails.DataSource = query;
            DV_EmployeeDetails.DataBind();
        }

        private void processOrders()
        {

            List<HRMService.OrderEntity> orders = (List<HRMService.OrderEntity>)ObjectDataSource2.Select();
            List<string> customers = new List<string>();
            int narocil = 0;
            decimal vrednost = 0;

            foreach (HRMService.OrderEntity o in orders)
            {
                if (!customers.Contains(o.Customer))
                    customers.Add(o.Customer);
                narocil += o.Amount;
                vrednost += o.TotalDue;
            }


            L_SaleStatistics1.Text = customers.Count.ToString();
            L_SaleStatistics2.Text = narocil.ToString();
            L_SaleStatistics3.Text = vrednost.ToString("C");
        }

        private void orderTagsVisibility(bool flag)
        {
            L_SaleStatistics4.Visible = flag;
            L_SaleStatistics5.Visible = flag;
            L_SaleStatistics6.Visible = flag;

            L_SaleStatistics1.Visible = flag;
            L_SaleStatistics2.Visible = flag;
            L_SaleStatistics3.Visible = flag;

            GV_Orders.Visible = flag;
        }
    }
}