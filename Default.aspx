﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63110513_Seminar2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 58px;
        }
        .auto-style2 {
            width: 160px;
        }
    </style>
    <link href="css/default.css" rel="stylesheet" type="text/css" id="stylesheet" runat="server" />
</head>
<body>
    <form id="form1" runat="server">        
        <hr />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Organizacijska enota:"></asp:Label>
        &nbsp;
        <asp:DropDownList ID="DDL_DepartmentGroup" runat="server" AutoPostBack="true" 
            OnSelectedIndexChanged="DDL_DepartmentGroup_SelectedIndexChanged">
        </asp:DropDownList><br />
        <asp:Label ID="Label2" runat="server" Text="Oddelek: " Font-Bold="True"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="DDL_Department" AutoPostBack="true"  runat="server" 
            OnSelectedIndexChanged="DDL_Department_SelectedIndexChanged">
        </asp:DropDownList>
        <hr />
        <br />
        <asp:GridView ID="GV_Employee" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" 
            PageSize="5" Width="100%" 
            OnSelectedIndexChanged="GV_Employee_SelectedIndexChanged" OnPageIndexChanged="GV_Employee_PageIndexChanging"
            GridLines="Horizontal" DataKeyNames="EmployeeID">
            <Columns>
                <asp:BoundField DataField="EmployeeID" Visible="False" />
                <asp:TemplateField HeaderText="Ime in priimek">
                    <ItemTemplate>
                        <div><%#Eval("FirstName")%> <%#Eval("LastName")%></div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="JobTitle" HeaderText="Delovno mesto" />
                <asp:BoundField DataField="HireDate" DataFormatString="{0:dd.MM.yyyy}" HeaderText="Datum zaposlitve">
                    <ItemStyle Width="15%"/>
                </asp:BoundField>
                <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" >
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                </asp:CommandField>
            </Columns>
            <HeaderStyle BackColor="Black" ForeColor="White" HorizontalAlign="Left" />
            <PagerStyle BackColor="Black" ForeColor="White" />
            <SelectedRowStyle CssClass="backlight border" />
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetEmployeeListForDepartment" TypeName="_63110460_Seminar2.HRMService.HRMServiceClient">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Department" Name="departmentID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:DetailsView ID="DV_EmployeeDetails" runat="server" AutoGenerateRows="False" 
            Height="50px" Width="100%" GridLines="None"
            FooterText="sth" HeaderText="sth" Visible="False">
            <FieldHeaderStyle Width="20%" BackColor="#999999" VerticalAlign="Top" />
            <Fields>
                <asp:TemplateField HeaderText="Ime in priimek:">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                        &nbsp;<asp:Label ID="Label4" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Spol:">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("Gender") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Datum rojstva:">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("BirthDate", "{0:dd.MM.yyyy}") %>'></asp:Label>
                        <br />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
                <asp:TemplateField HeaderText="Naslov:">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                        <br />
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                        &nbsp;<asp:Label ID="Label15" runat="server" Text='<%# Eval("PostalCode") %>'></asp:Label>
                        <br />
                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("State") %>'></asp:Label>
                        <br />
                        <asp:Label ID="Label9" runat="server" Text='<%# Eval("Country") %>'></asp:Label>
                        <br />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
                <asp:TemplateField HeaderText="Telefonska številka:">
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Elektronski naslov:">
                    <ItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("EmailAdress") %>'></asp:Label>
                        <br />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
                <asp:TemplateField HeaderText="Datum zaposlitve:">
                    <ItemTemplate>
                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("HireDate", "{0:dd.MM.yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delovna doba:">
                    <ItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("WorkYears") %>'></asp:Label> let
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delovno mesto:">
                    <ItemTemplate>
                        <asp:Label ID="Label14" runat="server" Text='<%# Eval("JobTitle") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Izmena:">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Shift") %>'></asp:Label>
                        &nbsp;(<asp:Label ID="Label18" runat="server" Text='<%# Eval("ShiftStartTime") %>'></asp:Label>
                        -<asp:Label ID="Label16" runat="server" Text='<%# Eval("ShiftEndTime") %>'></asp:Label>
                        )<br />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
                <asp:TemplateField HeaderText="Urna postavka:">
                    <ItemTemplate>
                        <asp:Label ID="Label17" runat="server" Text='<%# Eval("PayRate", "{0:C}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Fields>
            <FooterStyle BackColor="Black" />
            <HeaderStyle BackColor="Black" />
        </asp:DetailsView>
        <hr />
        <asp:Button ID="B_Orders" runat="server" Text="Prikaži ustvarjen promet" Visible="False" OnClick="B_Orders_Click" />
        <br />
        <br />
        <asp:Label ID="L_SaleStatistics4" runat="server" Text="Število strank: " Visible="False"></asp:Label>
        <asp:Label ID="L_SaleStatistics1" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics5" runat="server" Text="Število naročil: " Visible="False"></asp:Label>
        <asp:Label ID="L_SaleStatistics2" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics6" runat="server" Text="Skupna vrednost naročil: " Visible="False"></asp:Label>
        <asp:Label ID="L_SaleStatistics3" runat="server" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GV_Orders" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
            GridLines="None" PageSize="20" Width="100%" Visible="False" 
            DataSourceID="ObjectDataSource2">
            <Columns>
                <asp:BoundField DataField="OrderNumber" HeaderText="Naročilo" SortExpression="OrderNumber" >
                <HeaderStyle Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="OrderDate" HeaderText="Datum" SortExpression="OrderDate" DataFormatString="{0:dd.MM.yyyy}" >
                <HeaderStyle Width="10%" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Customer" HeaderText="Stranka" SortExpression="Customer" >
                <HeaderStyle Width="50%" />
                </asp:BoundField>
                <asp:BoundField DataField="Amount" HeaderText="Število izdelkov" SortExpression="Amount" >
                <HeaderStyle HorizontalAlign="Right" Width="6%" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="SubTotal" HeaderText="Vrednost" SortExpression="SubTotal" DataFormatString="{0:C}" >
                <HeaderStyle HorizontalAlign="Right" Width="6%" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="TaxAmt" HeaderText="Davek" SortExpression="TaxAmt" DataFormatString="{0:P}" >
                <HeaderStyle HorizontalAlign="Right" Width="6%" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="Freight" HeaderText="Dostava" SortExpression="Freight" DataFormatString="{0:C}" >
                <HeaderStyle HorizontalAlign="Right" Width="6%" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="TotalDue" HeaderText="Za plačilo" SortExpression="TotalDue" DataFormatString="{0:C}" >
                <HeaderStyle HorizontalAlign="Right" Width="6%" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <HeaderStyle BackColor="Black" ForeColor="White" />
            <PagerStyle BackColor="Black" ForeColor="White" />
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetOrderList" TypeName="_63110460_Seminar2.HRMService.HRMServiceClient">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Employee" Name="employeeID" PropertyName="SelectedValue" Type="Int32"/>
            </SelectParameters>
        </asp:ObjectDataSource>        
    </form>
</body>
</html>
